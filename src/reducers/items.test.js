import reducer from './items'
import * as actions from '../actions/index'

describe('Git hub items reducer', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual(
      {
        items: [],
        loading: false,
        error: null,
        query: "bootstrap",
        page:1
      }
    )
  })

  it('should handle PAGINATION_START', () => {
    expect(
      reducer([], {
        type: actions.PAGINATION_START,
        page: 1
      })
    ).toEqual(
      {
        page: 1
      }
    )
  })
  it('should handle SEARCH_BEGINS', () => {
    expect(
      reducer([], {
        type: actions.SEARCH_BEGINS,
        loading: true,
        error: null,
        items: []
      })
    ).toEqual(
      {
        loading: true,
        error: null,
        items: []
      }
    )
  })
  it('should handle SEARCH_RESULT_SUCCESS', () => {
    expect(
      reducer({loading:false}, {
        type: actions.SEARCH_RESULT_SUCCESS,
        payload: {items:['test']},
        query:'bootstrap'
      })
    ).toEqual(
      {loading:false,query:'bootstrap',items:['test']}
    )
  })
})