import {
  PAGINATION_START,
  SEARCH_BEGINS,
  SEARCH_RESULT_SUCCESS,
  SEARCH_RESULT_FAILURE,
  SEARCH_QUERY
} from "../actions";

const initialState = {
  items: [],
  loading: false,
  error: null,
  query: "bootstrap",
  page:1
};

export default function Items(
  state = initialState,
  action
) {
  switch (action.type) {
    case PAGINATION_START:
      return {
        ...state,
        page: action.page
      };
    case SEARCH_BEGINS:
      return {
        ...state,
        loading: true,
        error: null,
        items: [],

      };
    case SEARCH_RESULT_SUCCESS:
      // Mark the state as "loading" so we can show a spinner or something
      // Also, reset any errors. We're starting fresh.
      return {
        ...state,
        loading: false,
        items: action.payload.items,
        query: action.query,
        total_count: action.total_count
      };
    case SEARCH_RESULT_FAILURE:
      // Mark the state as "loading" so we can show a spinner or something
      // Also, reset any errors. We're starting fresh.
      return {
        ...state,
        loading: false,
        error: action.payload.error,
        query: null,
        items: []
      };
    case SEARCH_QUERY:
      return {
        ...state,
        query: action.query,
        page:1
      };

    default:
      // ALWAYS have a default case in a reducer
      return state;
  }
}
