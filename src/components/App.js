import React from 'react';
//import { StyleSheet, Text, View } from 'react-native';
import Header from './Header';
import SearchBar from '../containers/SearchBar';
import SearchResults from '../containers/SearchResults';
import Pagination from '../containers/Pagination';

import {createStore} from 'redux';
import {Provider} from 'react-redux';
import rootReducer from '../reducers';

// const store = createStore(rootReducer);
// store.subscribe(() => console.log('store', store.getState()));

export default class App extends React.Component {
  render() {
    return (
    	<div>
       	  <Header/>
          <SearchBar/>
          <SearchResults/>
          <Pagination />
          </div>
        
    );
  }
}
