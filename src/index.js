import React from "react";
import ReactDOM from "react-dom";
import App_Main from "./components/App";
import { createStore, applyMiddleware } from "redux";
import { Provider } from "react-redux";
import thunk from "redux-thunk";
import combineReducers from "./reducers";


const store = createStore(
  combineReducers,
  applyMiddleware(thunk)
);

function App() {
  return (
    <div className="App">
      <App_Main />
    </div>
  );
}

const rootElement = document.getElementById("root");
ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  rootElement
);

