import * as actions from './index'
import fetchMock from 'fetch-mock'
import expect from 'expect' 
import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'


const middlewares = [thunk]
const mockStore = configureMockStore(middlewares)

describe('actions', () => {
  it('should create an action searchQuery', () => {
    const query = 'bootstrap1'
    const expectedAction = {
      type: actions.SEARCH_QUERY,
      query
    }
    expect(actions.searchQuery(query)).toEqual(expectedAction)
  })
  it('should create an action searchBegin', () => {
    const expectedAction = {
      type: actions.SEARCH_BEGINS,
    }
    expect(actions.searchBegin()).toEqual(expectedAction)
  })
  it('should create an action paginatedData', () => {
  	const page = '1'
    const expectedAction = {
      type: actions.PAGINATION_START,
      page: page
    }
    expect(actions.paginatedData('1')).toEqual(expectedAction)
  })

})
describe('Fetch git Data', () => {
  afterEach(() => {
    fetchMock.restore()
  })

  it('creates SEARCH_ITEM_SUCCESS when fetching todos has been done', () => {
    const store = mockStore({ items: [] })

    return store.dispatch(actions.fetchItems('bootstrap','1')).then(() => {
      expect(store.getActions()).toMatchSnapshot();
    })
  })
})