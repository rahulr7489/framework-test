const axios = require('axios');


export const SEARCH_BEGINS = "SEARCH_BEGINS";
export const SEARCH_RESULT_SUCCESS = "SEARCH_RESULT_SUCCESS";
export const SEARCH_RESULT_FAILURE = "SEARCH_RESULT_FAILURE";
export const SEARCH_QUERY = "SEARCH_QUERY";
export const PAGINATION_START = "PAGINATION_START";

export const fetchItemSuccess = (items,query, count) => ({
  type: SEARCH_RESULT_SUCCESS,
  payload: { items },
  query: query,
  total_count: count
});

export const fetchItemFailure = error => ({
  type: SEARCH_RESULT_FAILURE,
  payload: { error }
});
export const searchQuery = query => ({
  type: SEARCH_QUERY,
  query: query
})
export const paginatedData = page => ({
  type: PAGINATION_START,
  page: page
})
export const searchBegin = () => ({
  type: SEARCH_BEGINS
});


function getUser(query,page){
  return axios.get('https://api.github.com/search/repositories?q='+query+'&page='+page);
}


export function fetchItems(query,page) {
  return dispatch => {
    dispatch(searchBegin());
    dispatch(searchQuery(query));
    dispatch(paginatedData(page));
    return getUser(query,page)
      .then(json => {
        dispatch(fetchItemSuccess(json.data.items,query, json.data.total_count));
        return json.data.items;
      })
      .catch(error =>
        dispatch(fetchItemFailure(error))
      );
  };
}

// Handle HTTP errors since fetch won't.
function handleErrors(response) {
  if (!response.ok) {
    throw Error(response.statusText);
  }
  return response;
}