import SearchBar from './SearchBar';
import React from 'react'
import Adapter from 'enzyme-adapter-react-16';
import { shallow, configure } from 'enzyme';
import renderer from 'react-test-renderer';
import * as actions from '../actions/index'
import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import fetchMock from 'fetch-mock'


const middlewares = [thunk]
const mockStore = configureMockStore(middlewares)

configure({adapter: new Adapter()});


describe('SearchBar Rendering', () => {
  let store;
  let wrapper;
  const initialState = { items: [],
  loading: false,
  error: null,
  query: "bootstrap",
  page:1 };

  it('intial rendering check', () => {
	  store = mockStore(initialState);
	  store.dispatch(actions.fetchItems('bootstrap','1'))
	  wrapper = shallow(<SearchBar store={store}/>);
	  expect(wrapper.props().store.getState().page).toBe(initialState.page);
	  expect(wrapper.props().store.getState().query).toBe(initialState.query);
  });
  it('search string', () => {
	  const store = mockStore({ items: [] })
	  store.dispatch(actions.fetchItems('bootstrap','1'))
	  wrapper = shallow(<SearchBar store={store}/>);
	  expect(wrapper.props().store.getActions()).toMatchSnapshot();
  });
  
  
});
