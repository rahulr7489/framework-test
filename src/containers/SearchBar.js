import React from 'react'
import { connect } from 'react-redux'
import {Nav,NavDropdown, Form, Button, FormControl, Col, InputGroup} from 'react-bootstrap'
import '../App.css';
import { fetchItems, searchQuery } from "../actions";


class SearchBar extends React.Component {
  constructor(props) {    
      super(props);
  }
  handleChnage(e){
    this.props.dispatch(searchQuery(e.target.value));
  }
  handleClick(e){
    this.props.dispatch(fetchItems(this.props.query,this.props.page));
  }
  render(){
      return (
        <main role="main" className="container">
          <div className="container searchbar">
            <div className="row">
              <div className="col-10">
                <input type="email" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Search" onChange={this.handleChnage.bind(this)}/>
              </div>
              <div className="col-2">
                <button className="btn btn-primary" onClick={this.handleClick.bind(this)}>Search</button>
              </div>
            </div>
          </div>
        </main>
  )
  }
}
const mapStateToProps = state => ({
  query: state.items.query,
  items: state.items.items,
  loading: state.items.loading,
  error: state.items.error,
  page:state.items.page
});

export default connect(mapStateToProps)(SearchBar);

