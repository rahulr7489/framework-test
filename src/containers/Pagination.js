import React from 'react'
import { connect } from 'react-redux'
import Pagination from 'react-bootstrap/Pagination'
import '../App.css';
import { fetchItems, searchQuery } from "../actions";


class Paginations extends React.Component {
  constructor(props) {    
      super(props);
  }

  handleClick(number,e){
    this.props.dispatch(fetchItems(this.props.query,number));
  }
  render(){
      let active = this.props.page;
      let total_page = Math.ceil(this.props.total_count / 30); 
      let items = [];
      if(this.props.items.length > 0){
      let page_start = active > 3 ? active - 2 : 1;
      let page_end = active > 3 ? active + 2 : 5;
      if(page_end > total_page){ page_end = total_page;}
      for (let number = page_start; number <= page_end; number++) {
      items.push(
        <Pagination.Item key={number} active={number === active} onClick={this.handleClick.bind(this,number)}>
          {number}
        </Pagination.Item>,
      );

      }
      }
      return (
        <div className="row search-result-items" >
        <div className="col-5">
        </div>
        <div className="col-2">
        <Pagination>{items}</Pagination>
        </div>
        <div className="col-5">
        </div>
        </div>
  )
  }
}
const mapStateToProps = state => ({
  query: state.items.query,
  items: state.items.items,
  loading: state.items.loading,
  error: state.items.error,
  page:state.items.page,
  total_count: state.items.total_count
});

export default connect(mapStateToProps)(Paginations);

