import React, {Component} from 'react';
//import {Linking, Text, TouchableOpacity, View} from 'react-native';
import '../App.css';
import {connect} from 'react-redux';
import { fetchItems, searchQuery } from "../actions";
import { faHome,faCodeBranch, faStar, faIdCard, faEye } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";


//const styles = require('./SearchResultsStyles');

class SearchResults extends Component {
  constructor(props) {
    super(props);
  }
  componentDidMount() {
    this.props.dispatch(fetchItems(this.props.query,this.props.page));
  }
  render() {
     const { error, loading, items } = this.props;
     if (loading) {
      return (<div className="container search-result"><div className="row search-result-items">Loading...</div></div>);
    }
    return(
      <div className="container search-result">
       <div>
    
  </div>
      {items.length > 0 ? items.map(item => (
        <div className="row search-result-items" key={item.id}>
          <div className="col-3">
            <dt className="text-truncate"><a href={item.url} target="_blank">{item.name}</a></dt>
          </div>
           <div className="col-3">
            <dt className="text-truncate">{item.language}</dt>
          </div>
          <div className="col-6">
          <div className="row left-align">
          <div className="col-sm-3">
            <FontAwesomeIcon icon={faCodeBranch}/><span className="count-span">{item.forks_count}</span>
          </div>
          <div className="col-sm-3">
            <FontAwesomeIcon icon={faIdCard}/><span className="count-span">{item.open_issues_count}</span>
          </div>
          <div className="col-sm-3">
            <FontAwesomeIcon icon={faStar}/><span className="count-span">{item.stargazers_count}</span>
          </div>
          <div className="col-sm-3">
            <FontAwesomeIcon icon={faEye}/><span className="count-span">{item.watchers_count}</span>
          </div>
          </div>
          </div>

        </div>
        )): <div className="row search-result-items">No Result Found!!</div>}
        
       
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    query: state.items.query,
    items: state.items.items,
    loading: state.items.loading,
    error: state.items.error,
    page:state.items.page
  };
}

export default connect(mapStateToProps, null)(SearchResults);
