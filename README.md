# Git Hub Search (React, Redux)

## How to Run the App
* clone the repo
* cd project folder
* npm install
* npm start
* for running test npm test


## Note 
* Added basic unit test case for containers, actions, reducers
* pagination has been implemented.
* unit test case is written in Jest